package org.duyhv.learning.integration.service.Impl;

import org.duyhv.learning.integration.domain.MqttDomain;
import org.duyhv.learning.integration.service.IMqttDomainService;
import org.springframework.stereotype.Service;

@Service
public class MqttDomainService implements IMqttDomainService {

    @Override
    public MqttDomain configMqttDomain(String brokerUri, String topic, String username, String password, int qos) {
        MqttDomain mqttDomain = new MqttDomain();
        mqttDomain.setBrokerUri(brokerUri);
        mqttDomain.setTopic(topic);
        mqttDomain.setUsername(username);
        mqttDomain.setPassword(password);
        mqttDomain.setQos(qos);
        return mqttDomain;
    }
}
