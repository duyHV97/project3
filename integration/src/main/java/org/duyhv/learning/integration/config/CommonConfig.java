package org.duyhv.learning.integration.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableIntegration
public class CommonConfig {
    @Bean(name = "CommonErrorChannel")
    public static MessageChannel commonErrorChannel() {
        return new DirectChannel();
    }
}

