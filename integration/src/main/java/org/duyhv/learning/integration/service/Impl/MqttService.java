package org.duyhv.learning.integration.service.Impl;

import lombok.extern.slf4j.Slf4j;
import org.duyhv.learning.integration.service.IMqttService;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.Publisher;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@EnableIntegration
public class MqttService implements IMqttService {


    @Override
    @Bean
    @ServiceActivator(inputChannel = "directChannel", outputChannel = "mqttServiceActivator")
    public MessageHandler mqttMessageHandler() {
        return new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                log.info("Message with payload: "+ message.getPayload() +", and header: "+ message.getHeaders());
            }
        };
    }

//    @Override
//    public String publishAMessage(String message) {
//        Publisher publisher= new Publisher();
//        return null;
//    }
}
