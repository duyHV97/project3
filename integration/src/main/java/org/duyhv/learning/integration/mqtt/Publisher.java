package org.duyhv.learning.integration.mqtt;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.paho.client.mqttv3.*;

@Getter
@Setter
public class Publisher {
    private String topic;
    private String content;
    private int qos;
    private String broker;
    private String clientId;

    private MqttClient client;

    public Publisher(String topic, String content, int qos, String broker) throws MqttException {
        this.topic= topic;
        this.content= content;
        this.qos= qos;
        this.broker= broker;
        this.clientId= MqttClient.generateClientId();
        client= new MqttClient(broker, clientId);
    }

    public void publish() throws MqttException{
        MqttTopic topic= client.getTopic(this.topic);
        topic.publish(new MqttMessage(this.content.getBytes()));
        System.out.println("Published message on topic: "+ topic.getName()+" : "+ this.content);
    }

    public boolean connect(){
        try {
            MqttConnectOptions connectOptions= new MqttConnectOptions();
            connectOptions.setCleanSession(false);
            connectOptions.setWill(client.getTopic(topic), "I'm gone... Bye!".getBytes(), 0, false);
            client.connect(connectOptions);
            return client.isConnected();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }
}
