package org.duyhv.learning.integration.domain;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class MqttDomain {

    private String brokerUri;
    private String topic;
    private String username;
    private String password;
    private int qos;

}
