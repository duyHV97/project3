package org.duyhv.learning.integration.config.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.duyhv.learning.integration.config.CommonConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class KafkaConfig {
    @Bean(name = "kafkaChannel")
    public MessageChannel kafkaChannel(){
        return new DirectChannel();
    }

    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaBootstrapServer;

    @Bean
    public ProducerFactory<String, Object> producerFactory(){
        Map<String, Object> props= new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, this.kafkaBootstrapServer);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<String, Object>(props);
    }
    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate(){
        return new KafkaTemplate<String, Object>(producerFactory());
    }
    @Bean
    @ServiceActivator(inputChannel = "directChannel")
    public MessageHandler handler() throws Exception{
        KafkaProducerMessageHandler<String, Object> producerMessageHandler= new KafkaProducerMessageHandler<>(this.kafkaTemplate());
        producerMessageHandler.setTopicExpression(new LiteralExpression("test"));
        producerMessageHandler.setMessageKeyExpression(new LiteralExpression("key"));
        producerMessageHandler.setSendFailureChannel(CommonConfig.commonErrorChannel());
        producerMessageHandler.setOutputChannel(this.kafkaChannel());
        return producerMessageHandler;
    }

    @Bean
    public IntegrationFlow kafkaFlow(){
        return IntegrationFlows.from("kafkaChannel")
                .transform(msg-> msg)
                .handle(new MessageHandler() {
                    @Override
                    public void handleMessage(Message<?> message) throws MessagingException {
                        log.info("Kafka message: "+ message.getPayload());
                    }
                })
                .get();
    }

    @ServiceActivator(inputChannel = "mongodbQueryFlow")
    public MessageHandler mongodbAsConsumer() throws Exception{
        KafkaProducerMessageHandler<String, Object> producerMessageHandler= new KafkaProducerMessageHandler<>(this.kafkaTemplate());
        producerMessageHandler.setTopicExpression(new LiteralExpression("mongodb"));
        producerMessageHandler.setMessageKeyExpression(new LiteralExpression("key"));
        producerMessageHandler.setSendFailureChannel(CommonConfig.commonErrorChannel());
        return producerMessageHandler;
    }
}
