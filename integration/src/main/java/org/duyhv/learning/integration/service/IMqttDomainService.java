package org.duyhv.learning.integration.service;

import org.duyhv.learning.integration.domain.MqttDomain;

public interface IMqttDomainService {
    MqttDomain configMqttDomain(String brokerDomain, String topic, String username, String password, int qos);
}
