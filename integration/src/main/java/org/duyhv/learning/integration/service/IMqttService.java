package org.duyhv.learning.integration.service;

import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Service;

@Service
@EnableIntegration
@IntegrationComponentScan
public interface IMqttService {
    @ServiceActivator(inputChannel = "directChannel")
    MessageHandler mqttMessageHandler();

//    public String publishAMessage(String message);
}