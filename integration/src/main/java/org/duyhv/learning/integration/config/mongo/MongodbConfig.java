package org.duyhv.learning.integration.config.mongo;

import lombok.extern.slf4j.Slf4j;
import org.duyhv.learning.integration.domain.Msg;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.mongodb.dsl.MongoDb;
import org.springframework.integration.mongodb.dsl.MongoDbOutboundGatewaySpec;
import org.springframework.integration.mongodb.outbound.MongoDbStoringMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Slf4j
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class MongodbConfig {

    @Bean(name = "mongodbQueryFlow")
    public MessageChannel mongodbAsConsumer(){ return new DirectChannel(); }

    @Bean
    public MongoMappingContext mappingContext(){
        return new MongoMappingContext();
    }

    @Bean
    public MongoConverter converter() throws Exception{
        DbRefResolver dbRefResolver= new DefaultDbRefResolver(this.mongoDbFactory());
        return new MappingMongoConverter(dbRefResolver, this.mappingContext());
    }


    @Bean
    public MongoDbFactory mongoDbFactory(){
        log.info("Connecting to iot_analytics db....");
        return new SimpleMongoClientDbFactory("mongodb://localhost:27017/iot_analytics");
    }

    // subscribe into the directChannel from mqtt protocol, get message and save into the mongodb collection: environmental_parameters
    @Bean
    @ServiceActivator(inputChannel = "directChannel")
    public MessageHandler mongoOutboundChannel(){
        MongoDbStoringMessageHandler mongoDbStoringMessageHandler= new MongoDbStoringMessageHandler(this.mongoDbFactory());
        mongoDbStoringMessageHandler.setCollectionNameExpression(new LiteralExpression("environmental_parameters"));
        log.info("insert message to iot_analytics....");
        return mongoDbStoringMessageHandler;
    }

    // inbound channel, query to create data flow to feed into some channels :))
    private MongoDbOutboundGatewaySpec gatewaySpec() throws Exception {
        return MongoDb.outboundGateway(this.mongoDbFactory(), this.converter())
                .query("{}")
                .collectionName("environmental_parameters")
//                .collectionCallback((collection, requestMessage) -> collection.countDocuments())
                .entityClass(Msg.class)
                .expectSingleResult(true);
    }

    @Bean(name = "retrieveResultsFlow")
    public IntegrationFlow gatewaySingleQueryFlow(){
        return f-> {
            try {
                f
                        .handle(gatewaySpec())
//                        .channel(c-> c.queue("retrieveResults"))
                        .channel("mongodbQueryFlow");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

    }
}
