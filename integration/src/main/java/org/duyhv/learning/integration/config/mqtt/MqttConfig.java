package org.duyhv.learning.integration.config.mqtt;

import lombok.extern.slf4j.Slf4j;
import org.duyhv.learning.integration.domain.MqttDomain;
import org.duyhv.learning.integration.service.IMqttDomainService;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.util.UUID;

@Slf4j
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class MqttConfig {

    private IMqttDomainService mqttService;

    private MqttDomain mqttDomain;
    private UUID uuid= UUID.randomUUID();

    @Autowired
    public MqttConfig(IMqttDomainService mqttService) {
        this.mqttService = mqttService;
    }

    @Bean(name = "directChannel")
    public MessageChannel mqttDirectChannel() {
        return new DirectChannel();
    }

    @Bean(name = "errorChannel")
    public MessageChannel errorChannel() {
        return new DirectChannel();
    }

    @Bean(name = "mqttServiceActivator")
    public MessageChannel mqttServiceActivator(){
        return new DirectChannel();
    }

    private void mqttDomain(){
        this.mqttDomain= new MqttDomain();
        String brokerUri = "tcp://localhost:1883";
        String topic= "test";
        int qos= 1;
        this.mqttDomain= this.mqttService.configMqttDomain(brokerUri, topic, null, null, qos);
    }

    @Bean
    public MqttConnectOptions connectOptions(){
        this.mqttDomain();
        MqttConnectOptions options= new MqttConnectOptions();
        options.setServerURIs(new String[] {this.mqttDomain.getBrokerUri()});
        if (this.mqttDomain.getUsername() != null){
            options.setUserName(this.mqttDomain.getUsername());
            options.setPassword(this.mqttDomain.getPassword().toCharArray());
        }
        options.setConnectionTimeout(10000);
        options.setAutomaticReconnect(true);
        return options;
    }
    @Bean
    public MqttPahoClientFactory clientFactory(){
        DefaultMqttPahoClientFactory factory= new DefaultMqttPahoClientFactory();
        factory.setPersistence(new MemoryPersistence());
        factory.setConnectionOptions(this.connectOptions());
        return factory;
    }

    @Bean
    public MessageProducerSupport inboundAdapter(){
        MqttPahoMessageDrivenChannelAdapter adapter= new MqttPahoMessageDrivenChannelAdapter(
                this.uuid.toString(),
                this.clientFactory(),
                this.mqttDomain.getTopic());

        adapter.connectionLost(new Throwable());
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(this.mqttDomain.getQos());
        adapter.setErrorChannel(errorChannel());
        adapter.setOutputChannel(mqttDirectChannel());
        adapter.setCompletionTimeout(10000);
        return adapter;
    }

    @Bean(name = "inboundChannel")
    public IntegrationFlow inboundChannel(){
        return IntegrationFlows.from(this.inboundAdapter())
                .transform(msg-> msg)
                .handle(new MessageHandler() {
                    @Override
                    public void handleMessage(Message<?> message) throws MessagingException {
                        log.info("Mqtt message with payload: "+ message.getPayload() +", and header: "+ message.getHeaders());
                    }
                })
                .get();
    }

    @Bean
    public IntegrationFlow serviceActivatorChannel(){
        return IntegrationFlows.from("mqttServiceActivator")
                .transform(m-> m)
                .handle(new MessageHandler() {
                    @Override
                    public void handleMessage(Message<?> message) throws MessagingException {
                        log.info("Message from mqtt service activator: " +message.getPayload()+ " "+ message.getHeaders());
                    }
                })
                .get();
    }
}
