package org.duyhv.learning.project3.analytics.mongo_spark;

import com.mongodb.spark.rdd.api.java.JavaMongoRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.bson.Document;

public class MongoSpark  {
    public static void main(String[] args) {
        SparkSession session= SparkSession.builder()
                .master("local")
                .appName("mongodb spark")
                .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/iot_analytics.environmental_parameters")
                .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/iot_analytics.environmental_parameters")
                .getOrCreate();
        JavaSparkContext jsc= new JavaSparkContext(session.sparkContext());
        JavaMongoRDD<Document> rdd= com.mongodb.spark.MongoSpark.load(jsc);
        rdd.persist(StorageLevel.MEMORY_ONLY_SER());
        System.out.println(rdd.count());
//        System.out.println(rdd.first().toJson());
        rdd.foreach(document -> System.out.println(document.toString()));
        rdd.map(new Function<Document, Object>() {
            @Override
            public Object call(Document document) throws Exception {
                return null;
            }
        });
    }
}
