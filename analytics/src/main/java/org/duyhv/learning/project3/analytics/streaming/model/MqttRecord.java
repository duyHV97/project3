package org.duyhv.learning.project3.analytics.streaming.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MqttRecord implements Serializable {
    private String message;
}
