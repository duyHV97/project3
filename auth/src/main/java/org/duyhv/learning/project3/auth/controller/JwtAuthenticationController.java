package org.duyhv.learning.project3.auth.controller;

import org.duyhv.learning.project3.auth.model.Role;
import org.duyhv.learning.project3.auth.model.User;
import org.duyhv.learning.project3.auth.model.jwt.JwtRequest;
import org.duyhv.learning.project3.auth.model.jwt.JwtResponse;
import org.duyhv.learning.project3.auth.service.JwtUserDetailsService;
import org.duyhv.learning.project3.auth.service.UserService;
import org.duyhv.learning.project3.auth.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(value = "/auth")
public class JwtAuthenticationController {

    private AuthenticationManager authenticationManager;
    private JwtTokenUtil jwtTokenUtil;
    private UserService userService;
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    public JwtAuthenticationController(AuthenticationManager authenticationManager,
                                       JwtTokenUtil jwtTokenUtil,
                                       UserService userService,
                                       JwtUserDetailsService jwtUserDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    private void authenticate(String email, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody JwtRequest jwtRequest) throws Exception{
        this.authenticate(jwtRequest.getEmail(), jwtRequest.getPassword());
        final UserDetails userDetails= this.jwtUserDetailsService.loadUserByUsername(jwtRequest.getEmail());
        final String token= this.jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> register(@RequestBody User user){
        Role qlkv= new Role();
        qlkv.setName("qlkv");
        Set<Role> roles= new HashSet<>();
        roles.add(qlkv);
        user.setRoles(roles);
        return ResponseEntity.ok(this.userService.save(user));
    }

    @DeleteMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) throws Exception{
        SecurityContextHolder.clearContext();
        HttpSession session= request.getSession(false);
        if (session != null){
            session.invalidate();
        }
        if (null != request.getCookies()){
            for (Cookie cookie: request.getCookies()){
                cookie.setMaxAge(0);
            }
        }
        response.addHeader("Authorization", "");
        JwtResponse jwtResponse= new JwtResponse(response.getHeader("Authorization"));
        return new ResponseEntity<JwtResponse>(jwtResponse, HttpStatus.OK);
    }
}
