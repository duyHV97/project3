package org.duyhv.learning.project3.auth.filter;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.duyhv.learning.project3.auth.service.JwtUserDetailsService;
import org.duyhv.learning.project3.auth.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The JwtRequestFilter extends the Spring Web Filter OncePerRequestFilter class.
 * For any incoming request this Filter class gets executed.
 * It checks if the request has a valid JWT token.
 * If it has a valid JWT Token then it sets the Authentication in the context, to specify that the current user is authenticated.
 */
@Component
@Slf4j
public class JwtRequestFilter  extends OncePerRequestFilter {
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader= request.getHeader("Authorization");
        String email= null;
        String jwtToken= null;
        if (requestTokenHeader!= null && requestTokenHeader.startsWith("Bearer")){
            // remove "Bearer "
            jwtToken= requestTokenHeader.substring(7);
            try {
                email= jwtTokenUtil.getEmailFromToken(jwtToken);
            }
            catch (IllegalArgumentException e){
                log.error("Unable to get token");
            }
            catch (ExpiredJwtException e){
                log.error("JWT token has expired");
            }
        }
        else {
            log.warn("Jwt does not start with Bearer");
        }

        if (email!= null && SecurityContextHolder.getContext().getAuthentication()== null){
            UserDetails userDetails= this.jwtUserDetailsService.loadUserByUsername(email);
            // if token is valid configure Spring Security to manually set
            // authentication
            if (this.jwtTokenUtil.validateToken(jwtToken, userDetails)){
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken= new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // After setting the Authentication in the context, we specify
                // that the current user is authenticated. So it passes the
                // Spring Security Configurations successfully.
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request, response);
    }
}
