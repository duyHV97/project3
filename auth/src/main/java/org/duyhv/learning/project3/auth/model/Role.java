package org.duyhv.learning.project3.auth.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "role")
@Getter
@Setter
public class Role implements Serializable, GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Override
    @Transient // not to insert into table
    public String getAuthority() {
        return null;
    }


    @ManyToMany(targetEntity = User.class, mappedBy = "roles", cascade = CascadeType.ALL)
//    @JsonManagedReference
    private Set<User> users;
}
