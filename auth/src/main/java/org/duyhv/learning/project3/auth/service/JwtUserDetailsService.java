package org.duyhv.learning.project3.auth.service;

import org.duyhv.learning.project3.auth.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * JWTUserDetailsService implements the Spring Security UserDetailsService interface.
 * It overrides the loadUserByUsername for fetching user details from the database using the username.
 * The Spring Security Authentication Manager calls this method for getting the user details from the database
 * when authenticating the user details provided by the user.
 */

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;


// this place can add the role for authority
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user= this.userService.findUserByEmail(email); //find by email
        if (user== null){
            throw new UsernameNotFoundException("Not found with email: "+ email);
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }

//    public User save(User user){
//        User newUser= new User();
//        newUser.setEmail(user.getEmail());
//        newUser.setPassword(passwordEncoder.encode(new ));
//    }

    @Transactional
    public User save(User user){
        User newUser= new User();
        newUser.setFullname(user.getFullname());
        newUser.setDob(user.getDob());
        newUser.setAddress(user.getAddress());
        newUser.setPhoneNumber(user.getPhoneNumber());
        newUser.setEmail(user.getEmail());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        return this.userService.save(new User());
    }
}
