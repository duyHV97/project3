package org.duyhv.learning.project3.auth.service;

import org.duyhv.learning.project3.auth.model.User;
import org.duyhv.learning.project3.auth.repository.RoleRepository;
import org.duyhv.learning.project3.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RoleRepository roleRepository;


    public User findUserByEmail(String email){
        return this.userRepository.findUserByEmail(email);
    }

    @Transactional
    public User save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user= this.userRepository.save(user);
        return user;
    }

}
