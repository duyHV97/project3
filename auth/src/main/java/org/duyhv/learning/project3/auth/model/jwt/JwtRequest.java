package org.duyhv.learning.project3.auth.model.jwt;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JwtRequest implements Serializable {
    private static final long serialVersionUID = 5926468583005150707L;

    public JwtRequest() {

    }

    private String email;
    private String password;
}
