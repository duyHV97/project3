package org.duyhv.learning.management.service.impl;

import org.duyhv.learning.management.domain.Msg;
import org.duyhv.learning.management.repository.MsgRepository;
import org.duyhv.learning.management.service.IMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsgService implements IMsgService{
    private MsgRepository msgRepository;
    @Autowired
    public MsgService(MsgRepository msgRepository) {
        this.msgRepository = msgRepository;
    }

    @Override
    public List<Msg> findMsgByDeviceId(int deviceId) {
        return this.msgRepository.findMsgsByDeviceId(deviceId);
    }
}
