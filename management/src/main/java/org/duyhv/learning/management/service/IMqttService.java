package org.duyhv.learning.management.service;

public interface IMqttService {
    public String publishAMessage(String message);
}
