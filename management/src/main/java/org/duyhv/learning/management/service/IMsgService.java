package org.duyhv.learning.management.service;

import org.duyhv.learning.management.domain.Msg;

import java.util.List;

public interface IMsgService {
    List<Msg> findMsgByDeviceId(int deviceId);
}
