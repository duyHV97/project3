package org.duyhv.learning.management.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;

@Configuration
public class MongodbConfig {

    @Bean
    public MongoDbFactory mongoDbFactory(){
        return new SimpleMongoClientDbFactory("mongodb://localhost:27017/iot_analytics");
    }
}
