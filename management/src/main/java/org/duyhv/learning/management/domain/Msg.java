package org.duyhv.learning.management.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Document(collection = "environmental_parameters")
public class Msg {
    @Id
    private String messageId;
    private int deviceId;
    private String temperature;
    private String humidity;
}
