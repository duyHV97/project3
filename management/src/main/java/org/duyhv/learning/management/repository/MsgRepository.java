package org.duyhv.learning.management.repository;

import org.duyhv.learning.management.domain.Msg;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableMongoRepositories
public interface MsgRepository extends MongoRepository<Msg, String> {
    List<Msg> findMsgsByDeviceId(int deviceId);
}
