package org.duyhv.learning.management.service.impl;

import org.duyhv.learning.management.mqtt.Publisher;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.stereotype.Service;

@Service
public class MqttService {

    public String publishAMessage(String message) throws MqttException {
        Publisher publisher= new Publisher("test2", message, 1, "tcp://localhost:1883");
        publisher.connect();
        publisher.publish();
        return message;
    }
}
