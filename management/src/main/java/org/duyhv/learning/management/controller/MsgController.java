package org.duyhv.learning.management.controller;

import org.duyhv.learning.management.domain.Msg;
import org.duyhv.learning.management.service.IMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/msg")
public class MsgController {

    private IMsgService msgService;

    @Autowired
    public MsgController(IMsgService msgService) {
        this.msgService = msgService;
    }

    @GetMapping(value = "/byDeviceId/{id}")
    public List<Msg> findMsgByDeviceId(@PathVariable(value = "id")int id){
        return this.msgService.findMsgByDeviceId(id);
    }
}
