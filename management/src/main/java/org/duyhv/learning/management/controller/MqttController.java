package org.duyhv.learning.management.controller;

import lombok.extern.slf4j.Slf4j;
import org.duyhv.learning.management.service.impl.MqttService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@Slf4j
@RequestMapping(value = "/mqtt")
public class MqttController {

    private final MqttService mqttService;

    @Autowired
    public MqttController(MqttService mqttService) {
        this.mqttService = mqttService;
    }

    @PostMapping(value = "/publish", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> publishAMsg(@RequestParam String msg) throws MqttException {
        this.mqttService.publishAMessage(msg);
        return ResponseEntity.ok("sent msg: "+ msg);
    }
}
